package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Main {

    enum Color {green, red};

    public static void main(String[] args) {


        Color[] balls = new Color[200];

        for (int i = 0; i < 200; i++) {
//            balls[i]= Color.red;
            balls[i]= Color.green;
            if( Math.random() < 0.5) {
                balls[i]= Color.red;
            } else {
                balls[i]= Color.green;
            }
        }

        long startTime = System.nanoTime();
        reorder(balls);
        long endTime = System.nanoTime();

        for (int i = 0; i < balls.length; i++) {
            System.out.println(i+ " : " +balls[i]);
        }

        long timeElapsed = endTime - startTime;

        System.out.println("It took " + timeElapsed / 10000000 + " ms");
    }

    public static void reorder (Color[] balls) {
        // TODO!!! Your program here
        //Method 1
        //        Arrays.sort( balls, (o1, o2) -> o2.compareTo(o1));

        //Method2
//        int r = 0;
//        int g = 0;
//
//        for (Color el: balls) {
//            if(el.equals(Color.red)){
//                r++;
//            }else if (el.equals(Color.green)){
//                g++;
//            }
//        }
//
//        for (int i = 0; i < r+g; i++) {
//            if(i < r){
//                balls[i] = Color.red;
//            } else {
//                balls[i] = Color.green;
//            }
//
//        }

        //Method 3
        int start = 0;
        int end = 0;

        for (Color ball: balls) {
            if(ball.equals(Color.red)) {
                balls[start] = Color.red;
                start++;
            } else if (ball.equals(Color.green)){
                balls[balls.length-(end+1)] =  Color.green;
                end++;
            }
        }

        System.out.println("start " + start);
        System.out.println("end " + end);
        int total = start + end;
        System.out.println("total "+ total);
    }

}
